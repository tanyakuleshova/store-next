import {useState} from 'react';
import {useRouter} from "next/router";

export const usePagination = () => {
  const router = useRouter();
  const { page } = router.query;

  const [pageCount, setPageCount] = useState(1);
  const [currentPage, setCurrentPage] = useState(page || 1);

  const setCountHandler = (total, count) => {
    if(total > 0){
      let lastPage = Math.ceil(total / count);
      setPageCount(lastPage);
    }
  }
  return {currentPage, setCurrentPage, pageCount, setCountHandler};
};
