import {useContext} from "react";
import {AuthContext} from "@/context/auth-context";
import {useCartApi} from "@/hooks/api-hook";


export const useAddToCart = () => {
    const auth = useContext(AuthContext);
    const { addCartItem, changeCartItem, isLoading, error } = useCartApi();
    const addToCart = async (id, qty, sign="+") => {
        try {
            let data = { product_id: id, qty: qty, sign: sign, tmp_id: auth.tmp_id};
            const response = await addCartItem(data);
            if (response.success) {
                auth.setCartCount(response.qty);
            }
            return response;
        } catch (e) {
            console.log(e);
        }
    }

    const changeCartItemQty = async (id, qty) => {
        try {
            let data = {item_id: id, qty: qty, tmp_id: auth.tmp_id};
            const response = await changeCartItem(data);
            if (response.success) {
                auth.setCartCount(response.qty);
            }
            return response;
        } catch (e) {
            console.log(e);
        }
    }

    return { addToCart, changeCartItemQty };
};
