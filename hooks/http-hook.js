import {useState, useCallback, useRef, useEffect, useContext} from 'react';
import {AuthContext} from "../context/auth-context";
import {useRouter} from "next/router";
import process from "next/dist/build/webpack/loaders/resolve-url-loader/lib/postcss";

export const useHttpClient = () => {

  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState();

  const activeHttpRequests = useRef([]);
  // const navigate = useNavigate();
  const router = useRouter()
  const auth = useContext(AuthContext);

  const sendRequest = useCallback(

    async (url, method = 'GET', body = null, headers = {}) => {
      setIsLoading(true);
      const httpAbortCtrl = new AbortController();
      activeHttpRequests.current.push(httpAbortCtrl);

      try {
        const response = await fetch(url, {
          method,
          body,
          headers,
          signal: httpAbortCtrl.signal
        });

        const responseData = await response.json();

        activeHttpRequests.current = activeHttpRequests.current.filter(
          reqCtrl => reqCtrl !== httpAbortCtrl
        );
        setIsLoading(false);
        if (!response.ok) {

          if (response.status === 401) {
            auth.logout();
            // navigate("/login");
            await router.push('/login')
            return responseData;
          }
          if (response.status === 404) {
            responseData.message = "Not Found";
            return responseData;
          }
          if (response.status === 403) {
            if (responseData.verified === false) {
              responseData.message = "Not Verified";
            } else {
              responseData.message = "Forbidden";
            }
            return responseData;
          }

          if (response.status === 422) {
            return responseData;
          }

          throw new Error(responseData.message);
        }

        return responseData;
      } catch (err) {
        //todo write to logs
        setError(err.message);
        window.error = err.message
        // setTimeout(()=>{
        //   setError("");
        // }, 3000);
        setIsLoading(false);
        throw err;
      }
    },
    []
  );

  const clearError = () => {
    setError(null);
  };

  useEffect(() => {
    return () => {
      // eslint-disable-next-line
      activeHttpRequests.current.forEach(abortCtrl => abortCtrl.abort());
    };
  }, []);

  return { isLoading, error, sendRequest, clearError };
};
