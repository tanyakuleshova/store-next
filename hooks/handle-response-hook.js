import { useState, useCallback, useEffect } from 'react';

export const useHandleResponse = (error) => {

  const [validationMessage, setValidationMessage] = useState({});
  const [errorMessage, setErrorMessage] = useState(error);

  const handleErrors = (response) => {
    if(response.error === 'validation_error') {
      setValidationMessage(response.errors);
    }else{
      setErrorMessage(response.message)
    }
  }

  const resetErrors = () => {
    setValidationMessage({});
    setErrorMessage("");
  }


  return { validationMessage, errorMessage, handleErrors, resetErrors, setErrorMessage };
};
