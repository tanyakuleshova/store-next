import {AuthContext} from "@/context/auth-context";
import {useContext} from "react";
import {useHttpClient} from "./http-hook";

export const useAuthApi = () => {
  const auth = useContext(AuthContext);
  const { isLoading, sendRequest, error, clearError } = useHttpClient();
  const register = async (data) => {
    return await sendRequest(
        '/api/auth/register',
        'POST',
        JSON.stringify(data),
        {
          'Content-Type': 'application/json'
        }
    )
  }

  const login = async (data) => {
    return await sendRequest(
        '/api/auth/login',
        'POST',
        JSON.stringify(data),
        {
          'Content-Type': 'application/json'
        }
    )
  }

  const logout = async (data) => {
    return await sendRequest(
        '/api/auth/logout',
        'POST',
        JSON.stringify(data),
        {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + auth.token
        }
    )
  }

    const sendResetPswCode = async (data) => {
        return await sendRequest(
            '/api/auth/send_reset_code',
            'POST',
            JSON.stringify(data),
            {
                'Content-Type': 'application/json'
            }
        )
    }

    const verifyCodeResetPsw = async (data) => {
        return await sendRequest(
            '/api/auth/reset',
            'POST',
            JSON.stringify(data),
            {
                'Content-Type': 'application/json'
            }
        )
    }
    const sendEmailVerificationCode = async (data) => {
        return await sendRequest(
            '/api/auth/verify_email_send',
            'POST',
            JSON.stringify(data),
            {
                'Content-Type': 'application/json'
            }
        )
    }
    const verifyEmail = async (data) => {
        return await sendRequest(
            '/api/auth/verify_email',
            'POST',
            JSON.stringify(data),
            {
                'Content-Type': 'application/json'
            }
        )
    }

  return { isLoading, error, clearError, register, login, sendResetPswCode, verifyCodeResetPsw, logout, sendEmailVerificationCode, verifyEmail };
};

export const useProductsApi = () => {
    const auth = useContext(AuthContext);
    const { isLoading, sendRequest, error, clearError } = useHttpClient();
    const products = async (data) => {
        return await sendRequest(
            '/api/products',
            'GET',
            undefined,
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + auth.token
            }
        )
    }

    const main_menu = async () => {
        return await sendRequest(
            '/api/products/main_menu',
            'GET',
            undefined,
            {
                'Content-Type': 'application/json'
            }
        )
    }

    return { isLoading, error, clearError, products, main_menu };
};

export const useCartApi = () => {
    const auth = useContext(AuthContext);
    const { isLoading, sendRequest, error, clearError } = useHttpClient();
    const addCartItem = async (data) => {
        return await sendRequest(
            process.env.API_URL+'/cart/add',
            'POST',
            JSON.stringify(data),
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + auth.token
            }
        )
    }

    const cartQty = async () => {
        return await sendRequest(
            process.env.API_URL+'/cart/qty?tmp_id='+auth.tmp_id,
            'GET',
            undefined,
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + auth.token
            }
        )
    }

    const getCart = async () => {
        return await sendRequest(
            process.env.API_URL+'/cart?tmp_id='+auth.tmp_id,
            'GET',
            undefined,
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + auth.token
            }
        )
    }

    const changeCartItem = async (data) => {
        return await sendRequest(
            process.env.API_URL+'/cart/item/change',
            'POST',
            JSON.stringify(data),
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + auth.token
            }
        )
    }
    const deleteCartItem = async (data) => {
        return await sendRequest(
            process.env.API_URL+'/cart/item/delete',
            'DELETE',
            JSON.stringify(data),
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + auth.token
            }
        )
    }

    return { isLoading, error, clearError, addCartItem, cartQty, getCart, changeCartItem, deleteCartItem };
};
export const useDeliveryApi = () => {
    const { isLoading, sendRequest, error, clearError } = useHttpClient();
    const getDeliveryList = async () => {
        return await sendRequest(
            process.env.API_URL+'/delivery',
            'GET',
            undefined,
            {
                'Content-Type': 'application/json'
            }
        )
    }

    const searchDelivery = async (params) => {
        return await sendRequest(
            process.env.API_URL+'/delivery/search?'+params,
            'GET',
            undefined,
            {
                'Content-Type': 'application/json'
            }
        )
    }

    return { isLoading, error, getDeliveryList, searchDelivery };
};
