import { useState, useCallback, useEffect } from 'react';
import {v4 as uuid} from "uuid";
export const useAuth = () => {
  const [token, setToken] = useState(false);
  const [userData, setUserData] = useState(false);
  const [tmp_id, setTmpId] = useState(null);

  const login = useCallback((data, token) => {
    setToken(token);
    setUserData(data);

    //todo save cart to the user
    localStorage.setItem(
        'userData',
        JSON.stringify({ data: data, token: token})
    );
  }, []);

  const defineTmpId = useCallback(tmp_id => {
    setTmpId(tmp_id);
    localStorage.setItem('tmp_id', tmp_id);
  }, [])

  const logout = useCallback(() => {
    setToken(null);
    setUserData(null);

    localStorage.removeItem('userData');
  }, []);

  useEffect(() => {
    const storedData = JSON.parse(localStorage.getItem('userData'));
    const storedTmpId =  localStorage.getItem('tmp_id');

    if (storedData && storedData.token) {
      login(storedData.data, storedData.token);
    }

    if (storedTmpId) {
      defineTmpId(storedTmpId);
    } else {
      defineTmpId(uuid());
    }

  }, [login, defineTmpId]);

  return { token, userData, login, logout, tmp_id };
};
