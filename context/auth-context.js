import { createContext } from 'react';

export const AuthContext = createContext({
  isLoggedIn: false,
  userId: null,
  token: null,
  refresh_token: null,
  verifyData: null,
  userData: null,
  tmp_id: null,
  login: () => {},
  logout: () => {},
  verify: () => {},
  setCartCount: () => {},
  cartCount: 0
});
