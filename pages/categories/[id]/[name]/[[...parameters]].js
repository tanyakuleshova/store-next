import { useRouter } from 'next/router';
import ProductCard from "@/components/products/product-card";
import {usePagination} from "@/hooks/pagination-hook";
import Pagination from "@/components/elements/pagination";
import {useEffect, useRef, useState} from "react";
import Filters from "@/components/filters/filters";
import Bread from "@/components/elements/bread-crumbs";
import Description from "@/components/products/description";
const CategoryDetail = ({items, category, totalPage, perPage, attrFilters, breadCrumbs}) => {

    const router = useRouter();
    const { id, name, parameters } = router.query;
    const pageParam = parameters ? parameters[0] : '1';
    const filterParam = parameters && parameters[1] ? parameters[1] : null;
    const [ filtersQueryParam, setFiltersQueryParam ] = useState(filterParam);
    const [ currentPage, setCurrentPage ] = useState(pageParam);

    const [ products, setProducts ] = useState(items);
    const [ pathname, setPathname ] = useState(`/categories/${id}/${name}`);
    const {pageCount, setCountHandler} = usePagination();


    useEffect(() => {
        setCountHandler(totalPage, perPage);
    }, []);

    const afterApplyFilters = (response) => {
        setCurrentPage('1');
        setProducts(response.products);
        setCountHandler(response.products.total, response.products.per_page);
    }

    return (
        <div className="p-4 mx-auto" style={{"maxWidth": "1400px"}}>
            <Bread styleClass="mb-6 text-gray-700" breadCrumbs={breadCrumbs}/>
            <h1 className="text-2xl mb-12 font-bold text-center">{category?.lang?.name}</h1>

            <div className="flex-s-between">
                <div style={{"width": "10%"}}>
                    <Filters
                        attrFilters={attrFilters}
                        applyCallback={afterApplyFilters}
                        setFiltersQueryParam={setFiltersQueryParam}
                        url={pathname}
                        category_id={id}
                        currentPage={currentPage}
                    />
                </div>
                <div itemScope itemType="https://schema.org/ItemList"
                     style={{marginLeft: '-1rem', marginRight: '-1rem', width: "90%"}} className="item-row">
                    {products && products.data.map(item => (
                        <ProductCard data={item} key={item.id}/>
                    ))}
                </div>
            </div>

            <Pagination pageCount={pageCount} currentPage={currentPage} url={pathname} filters={filtersQueryParam}/>

            <Description description={category?.lang?.description} styleClass='mt-6' defaultMode={false}/>
        </div>
    );
};

export default CategoryDetail;


export async function getServerSideProps(context) {
    const { params } = context;
    let page = params.parameters ? params.parameters[0] : '1';
    let url = process.env.API_URL + '/category/products?category_id=' + params.id + '&page='+ page;
    if(params.parameters && params.parameters[1]){
        url += '&filters='+params.parameters[1];
    }
    const response = await fetch(url);

    try{
        const res = await response.json();

        return {
            props: {
                items: res.products,
                category: res.category,
                totalPage: res.products.total,
                perPage: res.products.per_page,
                attrFilters: res.filters_data,
                breadCrumbs: res.bread
            }
        }
    }catch (e) {
        console.log(e.toString())
    }
    return {props: {items: {data: []}, category: {}, totalPage: 0, perPage: 0, attrFilters: {}, breadCrumbs: []}}
}
