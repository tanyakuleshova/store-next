import axios from "axios";

async function handler(req, res) {

    if (req.method !== 'GET') {
        return;
    }
    try{
        const response = await axios.get(process.env.API_URL+'/api/categories/menu');
        res.status(response.status).json(response.data)
    }catch (e){

        res.status(e.response.status).json(e.response.data)
    }

}
export default handler;
