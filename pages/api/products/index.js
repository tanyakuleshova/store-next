import axios from "axios";

async function handler(req, res) {
    if (req.method !== 'GET') {
        return;
    }
    const token = req.headers.authorization;
    try{
        const response = await axios.get('http://whalebase_api.local/api/product/list', {
            headers: {
                'Authorization': token
            }
        });
        res.status(response.status).json(response.data)
    }catch (e){
        res.status(e.response.status).json(e.response.data)
    }

}
export default handler;
