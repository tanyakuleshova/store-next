import axios from "axios";

async function handler(req, res) {
    if (req.method !== 'POST') {
        return;
    }
    const data = req.body;
    try{
        const response = await axios.post(process.env.API_URL+'/auth/password/send_reset_code', data);
        res.status(response.status).json(response.data)
    }catch (e){
        res.status(e.response.status).json(e.response.data)
    }

}
export default handler;
