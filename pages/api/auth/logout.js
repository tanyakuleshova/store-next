import axios from "axios";

async function handler(req, res) {
    if (req.method !== 'POST') {
        return;
    }
    const token = req.headers.authorization;
    const data = req.body;
    try {
        const response = await axios.post(process.env.API_URL+'/auth/logout', data,
            {
                headers: {
                    'Authorization': token
                }
            });

        res.status(response.status).json(response.data)
    } catch (e) {
        res.status(e.response.status).json(e.response.data)
    }
}

export default handler;
