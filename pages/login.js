import LoginForm from "@/components/auth/login-form";
import {AuthContext} from "@/context/auth-context";
import {useContext, useEffect, useRef, useState} from "react";
import Router from "next/router";
import RegisterForm from "@/components/auth/register-form";
import {motion} from 'framer-motion';
import Image from "next/image";
import SendResetCode from "@/components/auth/send-reset-code";
import VerifyCodeForm from "@/components/auth/verify-code-form";
import ResetPasswordForm from "@/components/auth/reset-password-form";
import VerifyEmailForm from "@/components/auth/verify-email-form";
import Header from "@/components/layout/header";
function LoginPage() {
  const auth = useContext(AuthContext);
  const [isLoginMode, setLoginMode] = useState(false);
  const [showForm, setShowForm] = useState('login');
  const [showRForm, setShowRForm] = useState('register');
  const loginDivRef  = useRef(null);
  const registerDivRef  = useRef(null);
  const cartImageRef  = useRef(null);
  const giftImageRef  = useRef(null);

  const [email, setEmail] = useState(null);
  const [code, setCode] = useState(null);
  const [message, setMessage] = useState("");

  useEffect(() => {
    if (auth.isLoggedIn) {
      Router.push('/');
    }
  }, [auth.isLoggedIn]);

  const onClickForgotPsw = ()=> {
    setShowForm('send_code');
  }

  const onClickSwitchToLogin = ()=> {
    setShowForm('login');
  }
  const onclickSwitchToVerify = ()=> {
    setShowForm('verify_code');
  }
  const onclickSwitchToResetPsw = ()=> {
    setShowForm('reset_psw');
  }
  const onClickSwitchToRegister = ()=> {
    setShowRForm('register');
  }
  const onclickSwitchToVerifyEmail = ()=> {
    setShowRForm('verify_email');
  }

  return <>
    <div className="absolute top-0">
      <Header/>
      {/*<SmallHeader/>*/}
    </div>
    <div className="flex">
      <div ref={loginDivRef} className="h-full w-1/2 min-h-screen flex-center" style={{"transition": "width 1.5s", "width": "70%"}}>
        {!isLoginMode && <motion.div initial="hidden" animate="visible" variants={{
          hidden: { scale: .8, opacity: 0 }, visible: { scale: 1, opacity: 1, transition: { delay: .4, duration: .5 }},
        }} className="mx-auto relative z-50">

          {message && <div className="text-yellow-600  p-4  text-center">{message}</div>}
          {showForm === 'login' &&
              <LoginForm
                  forgotPassword={true}
                  onclickForgotPassword={onClickForgotPsw}
              />}

          {showForm === 'send_code' &&
              <SendResetCode
                  backStep={onClickSwitchToLogin}
                  nextStep={onclickSwitchToVerify}
                  setEmail={setEmail}
              />}

          {showForm === 'verify_code' &&
              <VerifyCodeForm
                  backStep={onClickSwitchToLogin}
                  nextStep={onclickSwitchToResetPsw}
                  email={email}
                  setCode={setCode}
              />}
          {showForm === 'reset_psw' &&
              <ResetPasswordForm
                  nextStep={onClickSwitchToLogin}
                  email={email}
                  code={code}
                  setMessage={setMessage}
              />}

          <div className="flex my-4 text-gray-600 mb-28">
            <div className="mr-2 bg-yellow-100 p-2 rounded">Перейти до реєстрації?</div>

            <div onClick={() => {
              setLoginMode(true);
              onClickSwitchToRegister();
              registerDivRef.current.style.width = '70%';
              loginDivRef.current.style.width = '50%';
              cartImageRef.current.style.left = '0px';
              giftImageRef.current.style.left = '-200px'}
            } className="cursor-pointer underline text-yellow-600 bg-yellow-100 p-2 rounded">
              Створити акаунт
            </div>

          </div>
        </motion.div>}
        <motion.div initial={{x: -100}} animate={{ x: 0 }}
            className="overflow-hidden absolute bottom-8 left-0" style={{width: "1000px", height: "300px"}}>
          <Image ref={giftImageRef} height={0} width={0} sizes="100vw" src="/images/gift.png" alt="" className="absolute bottom-0"
                 style={{ width: 'auto', height: '200px', left: '0', transition: 'left 1s linear', opacity: '0.7'}}/>
        </motion.div>
      </div>

      <div ref={registerDivRef} style={{"transition": "width 1.5s"}}
           className="h-full bg-yellow-200 w-1/2 min-h-screen flex flex-col sm:justify-center">
        {isLoginMode && <motion.div initial="hidden" animate="visible" variants={{
          hidden: { scale: .8, opacity: 0 }, visible: { scale: 1, opacity: 1, transition: { delay: .4, duration: .5 }},
        }} className="mx-auto relative z-50">
          {showRForm === 'register' &&
              <RegisterForm
                  nextStep={onclickSwitchToVerifyEmail}
                  setEmail={setEmail}
              />}
          {showRForm === 'verify_email' &&
              <VerifyEmailForm
                  backStep={onClickSwitchToRegister}
                  email={email}
              />}
          <div className="flex-start my-4 text-gray-600 mb-28">
            <div className="mr-2 p-2 rounded bg-yellow-200">Вже є акаунт?</div>

            <div onClick={()=>{
              setLoginMode(false);
              onClickSwitchToLogin();
              loginDivRef.current.style.width = '70%';
              registerDivRef.current.style.width = '50%';
              cartImageRef.current.style.left = '300px';
              giftImageRef.current.style.left = '0px' }
            } className="cursor-pointer underline text-yellow-600 bg-yellow-100 shadow-md px-2 py-1 rounded-md">
              Вхід
            </div>

          </div>
        </motion.div>}
        <motion.div initial="hidden" animate="visible" variants={{
          hidden: { scale: .8, opacity: 0 }, visible: { scale: 1, opacity: 1, transition: { delay: .4, duration: .5 }} }}
                    className="overflow-hidden absolute bottom-0 right-0" style={{width: "587px", height: "200px"}}>
          <Image ref={cartImageRef} height={0} width={0} sizes="100vw" src="/images/cart.png" alt="" className="absolute"
                 style={{ width: 'auto', height: '200px', left: '300px', transition: 'left 1s linear', opacity: '0.8'}}/>
        </motion.div>
      </div>
    </div>
  </>;
}

export default LoginPage;
