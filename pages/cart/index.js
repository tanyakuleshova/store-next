import {useContext, useEffect, useMemo, useRef, useState} from "react";
import {AuthContext} from "@/context/auth-context";
import {useCartApi} from "@/hooks/api-hook";
import {v4 as uuid} from "uuid";
import ChangeQty from "@/components/products/change-qty";
import Image from "next/image";
import Modal from "@/components/elements/dialog/modal";
import Delivery from "@/components/cart/delivery";
function Cart() {
    const auth = useContext(AuthContext);
    const {getCart, deleteCartItem, isLoading} = useCartApi();

    const [cartItems, setCartItems] = useState(null);
    const [cartImages, setCartImages] = useState(null);
    const [showDeleteDialog, setShowDeleteDialog] = useState(false);
    const [currentItemId, setCurrentItemId] = useState('');

    useEffect(() => {
        if (auth.tmp_id) {
            getCart().then((res) => {
                getCartItems(res);
                getCartImages(res);
                auth.setCartCount(res.qty);
            });
        }
    }, [auth.tmp_id]);

    const getCartItems = (res) => {
        let _cartItems = [];
        res?.cart?.items?.map(item => {
            _cartItems.push({
                id: item.id,
                qty: item.qty,
                price: item.product?.price,
                sum: item?.product?.price * item.qty,
                product_id: item?.product?.id,
                name: item.product?.lang?.name,
                reference: item.product?.reference
            })
        })
        setCartItems(_cartItems);
    }
    const getCartImages = (res) => {
        let _cartImages = [];
        res?.cart?.items?.map(item => {
            _cartImages.push({url: item.product?.s3path, id: item.id})
        })
        setCartImages(_cartImages);
    }

    const cartQty = useMemo(() => {
        let qty = 0;
        let total = 0;

        if (cartItems) {
            cartItems.forEach(item => {
                qty += item?.qty || 0;
                total += (item?.qty || 0) * (item?.price || 0);
            });
        }
        return { qty, total };
    }, [cartItems]);


    const showDeleteDialogWindow = (item_id) => {
        setCurrentItemId(item_id);
        setShowDeleteDialog(true);
    }
    const deleteFromCart = async () => {
        const data = {item_id: currentItemId, tmp_id: auth.tmp_id};
        const res = await deleteCartItem(data);
        if(res.success){
            setShowDeleteDialog(false);
        }
    }

    return <>
        <div className="p-4 mx-auto" style={{"maxWidth": "1400px"}}>
            <Modal
                show={showDeleteDialog}
                className={"dialog bg-dialog_bg w-width_smx bg-backdrop"}
                onCancel={()=>{setShowDeleteDialog(false)}}
                headerClass={"p-4 bg-yellow-500 font-bold"}
                header={'Видалити товар із корзини?'}
                contentClass="p-4"
                footer={
                    <div className="p-4 flex items-center">
                        <button type="text" disabled={isLoading}
                             onClick={()=>{setShowDeleteDialog(false)}}>
                            Скасувати
                        </button>
                        <button type="text" disabled={isLoading}
                             onClick={deleteFromCart}>
                            Видалити
                        </button>
                    </div>
                }
            >
                <div dangerouslySetInnerHTML={{ __html:modalText }} className="w-full text-white"></div>
            </Modal>
            <h1 className="text-lg font-bold my-4">Кошик</h1>
            <div className="flex items-stretch justify-between">
                <div className="p-6 bg-white/20 shadow-lg w-2/3 h-full">

                    {cartItems && cartItems.length ? <div key={uuid()} className="flex items-center justify-start">
                        <div className="mr-2" style={{width: '250px'}}>Товар</div>
                        <div className="ml-3" style={{width: '80px'}}></div>
                        <div className="text-center" style={{width: '150px'}}>Кількість</div>
                        <div className="text-center" style={{width: '150px'}}>Ціна за одиницю</div>
                        <div className="text-center" style={{width: '150px'}}>Сума</div>
                    </div> : ""}


                    <div className={`flex items-center ${!cartItems || !cartItems.length ? 'justify-center' : 'justify-start'}`}>
                        <div>
                            {cartImages && cartImages.map(item => (
                                <div key={uuid()} style={{height: '100px'}} className="flex items-center">
                                    <div style={{width: '80px', height: '80px'}} className="mr-3 shadow-md my-3">
                                        <img className="rounded-md" src={item.url} alt=""
                                             style={{maxWidth: '100%', maxHeight: '100%'}}/>
                                    </div>
                                </div>
                            ))}
                        </div>
                        <div>
                            {cartItems && cartItems.map(item => (
                                <div key={uuid()} className="flex items-center justify-between text-lg py-2"
                                     style={{fontSize: "15px", height: '100px'}}>

                                    <div className="mr-2" style={{width: '250px'}}>
                                        <div>{item.name}</div>
                                        <div>{item.reference}</div>
                                    </div>
                                    <div className="text-center" style={{width: '150px'}}>
                                        <ChangeQty
                                            callback={updateCart}
                                            product_id={item.product_id}
                                            item_id={item.id}
                                            qty={item.qty}
                                            styleClass='text-sm my-4'
                                            btnClass="px-3 py-1"
                                        />
                                    </div>

                                    <div className="text-center" style={{width: '150px'}}>{item.price} грн</div>
                                    <div className="text-center"
                                         style={{width: '150px'}}>{item.sum} грн
                                    </div>
                                    <div>
                                        <Image
                                            onClick={()=>showDeleteDialogWindow(item.id)}
                                            title='Видалити'
                                            className="cursor-pointer"
                                            width={17}
                                            height={17}
                                            alt='close'
                                            src='/images/icons/close-red.svg'
                                        />
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
                <div className="w-1/3">
                    <div className="p-6 ml-6 bg-white/20 shadow-lg h-full"
                         style={!(cartItems && cartItems.length) ? {"minHeight": "80vh"} : {}}>
                        <div className="font-bold text-lg my-2 ">Разом</div>
                        <div className="flex justify-between items-center text-gray-700 text-sm">
                            <div>{cartQty.qty}</div>
                            <div>{cartQty.total} грн</div>
                        </div>
                    </div>
                </div>
            </div>
           <Delivery/>
        </div>
    </>;
}

export default Cart;
