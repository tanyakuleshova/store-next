import {useRouter} from "next/router";
import {v4 as uuid} from "uuid";
import Description from "@/components/products/description";
import Bread from "@/components/elements/bread-crumbs";
import {useState} from "react";
import Carousel from "@/components/products/carousel";
import ProductCard from "@/components/products/product-card";
import AddToCartBtn from "@/components/products/add-to-cart-btn";
import ChangeQty from "@/components/products/change-qty";

function Product({product, breadCrumbs, related}) {
  const router = useRouter();
  const { id, name } = router.query;
  const [activeTab, setActiveTab] = useState("attributes");
  const [qty, setQty] = useState(1);
  return <>
    <div style={{"maxWidth": "1400px"}} className="p-4 mx-auto text-black">
      <Bread styleClass="mb-6 text-gray-700" breadCrumbs={breadCrumbs} product={product}/>
      <div className="mb-12" style={{height: '32px'}}></div>
      <div className="p-4 bg-white/20 shadow-lg" >
        <div className="flex-s-between mt-4">

          <div className="w-1/2 pl-4 pr-12 mb-3">
            <Carousel mainUrl={product?.s3path} alt={product?.lang?.name} images={product?.s3images}/>
          </div>

          <div className="w-1/2 flex-center">
            <div className="w-full">
              <h1 className="text-2xl font-bold">{product?.lang?.name}</h1>
              <div className="text-md text-gray-700">Артикул: <span className="font-bold">{product.reference}</span></div>
              <div itemProp="offers" itemScope itemType="https://schema.org/Offer" className="flex-start mt-4">
                <div className="text-xl font-bold">
                  <span itemProp="price" content={product?.price}>{product?.price}</span>
                </div>
                <div className="ml-2 text-md">
                  <span itemProp="priceCurrency" content="UAH">грн</span>
                </div>
                <link itemProp="availability" href="https://schema.org/InStock" content="In stock"/>
              </div>

              <div className="flex-start">
                <ChangeQty
                    qty={qty}
                    setQty={setQty}
                    styleClass='mr-8 my-4'
                    btnClass="px-3 py-1"
                />
                <AddToCartBtn id={id} qty={qty} name="Додати у кошик"/>
              </div>

              <div className="flex items-end justify-start mt-4 w-full">
                {product?.attributes &&
                    <div onClick={()=>setActiveTab("attributes")}
                         className={`${activeTab === "attributes" ? "active-tab" : "inactive-tab"} tab-name`}>
                      Характеристики
                    </div>}
                {product?.lang?.description &&
                    <div onClick={()=>setActiveTab("description")}
                         className={`${activeTab === "description" ? "active-tab" : "inactive-tab"} tab-name`}>
                      Описання
                    </div>}
                <div className="w-full border-b border-yellow-500"></div>
              </div>

              <div className="flex-start mt-4 w-full">
                <div className={`${activeTab === "description" ? "w-full opacity-1" : "w-0 h-0 opacity-0"} 
                overflow-hidden`} style={{"transition": "opacity 1.2s"}}>
                  {product?.lang?.description &&
                      <Description styleClass='my-4' description={product?.lang?.description} defaultMode={false}/>}
                </div>
                <div className={`${activeTab === "attributes" ? "w-full opacity-1" : "w-0 h-0 opacity-0"} overflow-hidden`}
                     style={{"transition": "opacity 1.2s"}}>
                  {product?.attributes && product?.attributes.map(item => (
                      <div key={uuid()} className="flex-start my-1">
                        <div className="font-bold" style={{width: '120px'}}>{item?.attribute?.lang?.name}</div>
                        <div className="">{item?.value?.lang?.name}</div>
                      </div>
                  ))}
                </div>
              </div>
              {/*end of tabs*/}
            </div>
          </div>
        </div>
      </div>
      <div className="mb-12 mt-16 font-bold text-center text-2xl">Схожі товари</div>
      <div itemScope itemType="https://schema.org/ItemList"
           style={{marginLeft: '-1rem', marginRight: '-1rem', }} className="item-row">
      {related && related.map(item => (
          <ProductCard data={item} key={item.id} bg="bg-white/40"/>
      ))}
      </div>
    </div>
  </>;
}
export async function getServerSideProps(context) {
  const { params } = context;
  let url = process.env.API_URL + '/product?id=' + params.id;

  const response = await fetch(url);

  try{
    const res = await response.json();

    return {
      props: {
        product: res.product,
        breadCrumbs: res.bread,
        related: res.related
      }
    }
  }catch (e) {
    console.log(e.toString())
  }
  return {props: {product: {}, breadCrumbs: []}}
}

export default Product;
