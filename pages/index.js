function HomePage({ menu }) {

  return <>
      <div className="p-4 mx-auto" style={{"maxWidth": "1400px"}}>
        <div className="my-4 m-4 text-black text-md font-bold flex items-center justify-between">
          {menu.map(item => (
              <div key={item.id}>
                <a href={`/categories/${item.id}/${item.lang.slug}/1`}>{item.lang.name}</a>
              </div>
          ))}
        </div>
      </div>
  </>;
}

export default HomePage;

export async function getStaticProps() {
  const response = await fetch(process.env.API_URL + '/categories/menu');
  const res = await response.json();
  return {props: {menu: res.data}}
}
