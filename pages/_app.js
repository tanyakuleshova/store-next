import Layout from '../components/layout/layout';
import '../styles/globals.css';
import {useAuth} from "@/hooks/auth-hook";
import {AuthContext} from '@/context/auth-context';
import {useState} from "react";

function MyApp({Component, pageProps}) {
  const {token, userData, login, logout, tmp_id} = useAuth();
  const [cartCount, setCartCount] = useState(0);
  console.log(token);
  // if(token){
  return (
      <AuthContext.Provider
          value={{
              isLoggedIn: !!token,
              token: token,
              refresh_token: null,
              userId: null,
              userData: userData,
              verifyData: null,
              tmp_id: tmp_id,
              login: login,
              logout: logout,
              verify: null,
              setCartCount: setCartCount,
              cartCount: cartCount
          }}
      >
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </AuthContext.Provider>
  );
  // }else{
  //     return 401;
  // }

}

export default MyApp;
