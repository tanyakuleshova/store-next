import Link from "next/link";
import {v4 as uuid} from "uuid";
import {useEffect, useState} from "react";
import {useDeliveryApi} from "@/hooks/api-hook";
import Image from "next/image";

function Delivery(props) {

    const { getDeliveryList, searchDelivery, isLoading} = useDeliveryApi();
    const [ list, setList ] = useState();
    const [ selectedDelivery, setSelectedDelivery ] = useState(null);
    useEffect( () => {
        const getDelivery = async () => {
            try {
                const responseData = await getDeliveryList();
                const items = responseData.list;
                setList(items);
            } catch (err) {
                console.log(err);
            }
        };
        getDelivery();
    }, []);

    const searchSettlement = async (str) => {
        let param = 'settlement='+str;
        const responseData = await searchDelivery(param);
    }

    return <>
        <h1 className="text-lg font-bold my-4">Доставка</h1>
        <div className="p-6 bg-white/20 shadow-lg w-2/3 h-full">
            <div>
                <div>Населений пункт</div>
                <input
                    onChange={(e) => searchSettlement(e.target.value)}
                    className="classic-input" style={{ width: '400px' }}
                    placeholder="Почніть вводити ваш населений пункт"
                    type="text"
                />
            </div>
            {list && list.length && list.map(item => (
                <div key={uuid()} className="flex items-center justify-between">
                    <div className="flex items-center justify-start my-4">
                        <div className="mr-2 mt-1">
                            <input
                                checked={selectedDelivery === item.id}
                                type="checkbox"
                                value={item.id}
                                onChange={()=>{setSelectedDelivery(item.id)}}
                            />
                        </div>
                        <div className="text-sm">{item.title}</div>
                    </div>
                    <Image height={30} width={100} alt={item.slug} src={`/images/delivery/${item.slug}.svg`} />
                </div>
            ))}
        </div>

    </>;
}

export default Delivery;
