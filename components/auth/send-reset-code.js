import {useRef} from 'react';
import {useAuthApi} from "@/hooks/api-hook";
import Input from "@/components/elements/input";
import {useHandleResponse} from "@/hooks/handle-response-hook";
import Loading from "@/components/elements/loading";

function SendResetCode(props) {
  const { sendResetPswCode, isLoading, error } = useAuthApi();
  const { validationMessage, errorMessage, handleErrors, resetErrors } = useHandleResponse(error);

  const emailInputRef = useRef();

  async function submitHandler(event){
    event.preventDefault();
    resetErrors();

    try{
      const response = await sendResetPswCode({email: emailInputRef.current.value});
      props.setEmail(emailInputRef.current.value);
      if(response.success){
       props.nextStep();
      }else{
        handleErrors(response);
      }
    }catch (e){
      console.log(e);
    }
  }

  return (
      <>
        <section className="mx-auto">
          <div className="text-gray-600 font-bold text-lg my-3">Відправити код для відновлення паролю</div>
          <form onSubmit={submitHandler} className='rounded-lg p-6 shadow-md bg-yellow-200 form-w'>

            <Input label="Email" field='email'
                   inputType='email'
                   validationMessage={validationMessage}
                   reference={emailInputRef}
                   styleClass='yellow-200-autofill'
                   wrapperStyle='mb-3'
            />

            <div className="mb-3 text-sm text-red-500" style={{minHeight: '20px'}}>
              {errorMessage && <span>{errorMessage}</span>}
            </div>

            <div className='flex-center'>
              {props.backStep &&
                  <button disabled={isLoading} onClick={props.backStep} type="text"
                          className="text-sm mr-2 text-gray-600 hover:text-gray-800">
                    Згадали пароль?
                  </button>}
              <button disabled={isLoading} className='submit-btn'>Надіслати</button>
              <div className='w-6 ml-2'>{isLoading && <Loading/>}</div>
            </div>
          </form>
        </section>
      </>
  );
}

export default SendResetCode;
