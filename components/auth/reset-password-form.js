import {useRef, useState} from 'react';
import {useAuthApi} from "@/hooks/api-hook";
import Input from "@/components/elements/input";
import {useHandleResponse} from "@/hooks/handle-response-hook";
import Loading from "@/components/elements/loading";

function ResetPasswordForm(props) {

  const { verifyCodeResetPsw, isLoading, error } = useAuthApi();
  const { validationMessage, errorMessage, setErrorMessage, handleErrors, resetErrors } = useHandleResponse(error);

  const pswInputRef = useRef();
  const pswRepInputRef = useRef();

  async function submitHandler(event){
    event.preventDefault();
    resetErrors();

    try{
      if(pswInputRef.current.value !== pswRepInputRef.current.value){
        setErrorMessage("Пароль підтверджено невірно.");
      }else{
        const data = {email: props.email, code: props.code, password: pswInputRef.current.value};
        const response = await verifyCodeResetPsw(data);
        if(response.success){
          props.setMessage("Ви успішно відновили пароль!");

          setTimeout(()=>{
            props.setMessage("");
          }, 8000);

          props.nextStep();
        }else{
          handleErrors(response);
        }
      }
    }catch (e){
      console.log(e);
    }
  }

  return (
      <>
        <section className="mx-auto">
          <div className="text-gray-600 font-bold text-lg my-3">Введіть код для скидання паролю</div>
          <form onSubmit={submitHandler} className='rounded-lg p-6 shadow-md bg-yellow-200 form-w'>

            <Input label="Пароль" field='password'
                   inputType='password'
                   validationMessage={validationMessage}
                   reference={pswInputRef}
                   styleClass='yellow-200-autofill'
                   wrapperStyle='mb-7 '
            />

            <Input label="Повторіть пароль" field='repeat_password'
                   inputType='password'
                   validationMessage={validationMessage}
                   reference={pswRepInputRef}
                   styleClass='yellow-200-autofill'
                   wrapperStyle='mb-3'
            />

            {errorMessage && <div className="my-2 text-sm text-red-500">{errorMessage}</div>}

            <div className='flex-center'>
              {isLoading && <Loading styleClass="mr-4"/>}
              <button  disabled={isLoading} className='submit-btn'>Підтвердити</button>
            </div>
          </form>
        </section>
      </>
  );
}

export default ResetPasswordForm;
