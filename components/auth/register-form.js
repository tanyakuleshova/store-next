import { useRef } from 'react';
import { useAuthApi } from "@/hooks/api-hook";
import Input from "@/components/elements/input";
import { useHandleResponse } from "@/hooks/handle-response-hook";
import Loading from "@/components/elements/loading";
function RegisterForm(props) {

  const { register, isLoading, error } = useAuthApi();
  const { validationMessage, errorMessage, handleErrors, resetErrors, setErrorMessage } = useHandleResponse(error);

  const passwordInputRef = useRef();
  const confirmPasswordInputRef = useRef();
  const emailInputRef =useRef();

  async function submitHandler(event){
    event.preventDefault()

    resetErrors();

    const password = passwordInputRef.current.value;
    const confirmPassword = confirmPasswordInputRef.current.value;


    if(password !== confirmPassword){
      setErrorMessage('Паролі не співпадають');
    } else {
      try{
        const formData = new FormData(event.target);
        let formDataObject = Object.fromEntries(formData.entries());

        const response = await register(formDataObject);

        if(!response.success) {
          handleErrors(response);
        } else {
          props.setEmail(emailInputRef.current.value);
          props.nextStep();
        }
      } catch (e) {
        console.log(e);
        //todo
      }
    }
  }

  return (
      <>
        <section className="mx-auto">

          <div className="text-gray-600 font-bold text-lg my-3">Реєстрація</div>
          <form onSubmit={submitHandler} className='rounded-lg p-6 shadow-md bg-yellow-100 form-w'>

            <Input
                label="Email"
                field='email'
                inputType='email'
                validationMessage={validationMessage}
                reference={emailInputRef}
                styleClass='yellow-100-autofill'
                wrapperStyle='mb-4'
            />
            <Input
                label="Ім'я"
                field='first_name'
                validationMessage={validationMessage}
                styleClass='yellow-100-autofill'
                wrapperStyle='mb-4'
            />
            <Input
                label="Прізвище"
                field='last_name'
                validationMessage={validationMessage}
                styleClass='yellow-100-autofill'
                wrapperStyle='mb-4'
            />
            <Input
                label="Пароль"
                field='password'
                inputType='password'
                validationMessage={validationMessage}
                reference={passwordInputRef}
                styleClass='yellow-100-autofill'
                wrapperStyle='mb-4'
            />
            <Input
                label="Підтвердження паролю"
                inputType='password'
                reference={confirmPasswordInputRef}
                styleClass='yellow-100-autofill'
                wrapperStyle='mb-3'
            />

            <div className="mb-3 text-sm text-red-500" style={{minHeight: '20px'}}>
              {errorMessage && <span>{errorMessage}</span>}
            </div>

            <div className='flex-center'>
              <button disabled={isLoading} className='submit-btn'>Зареєструватися</button>
              <div className='w-6 ml-2'>{isLoading && <Loading/>}</div>
            </div>
          </form>
        </section>
      </>
  );
}

export default RegisterForm;
