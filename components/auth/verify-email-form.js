import {useContext, useRef} from 'react';
import {useAuthApi} from "@/hooks/api-hook";
import Input from "@/components/elements/input";
import {useHandleResponse} from "@/hooks/handle-response-hook";
import Loading from "@/components/elements/loading";
import {AuthContext} from "@/context/auth-context";
import Router from "next/router";

function VerifyEmailForm(props) {

  const auth = useContext(AuthContext);
  const { verifyEmail, sendEmailVerificationCode, isLoading, error } = useAuthApi();
  const { validationMessage, errorMessage, handleErrors, resetErrors } = useHandleResponse(error);

  const codeInputRef = useRef();

  async function submitHandler(event){
    event.preventDefault();
    resetErrors();
    try{
      const response = await verifyEmail({email: props.email, code: codeInputRef.current.value});
      if(response.success){
        auth.login(response.user, response.token);
        Router.push('/')
      }else{
        handleErrors(response);
      }
    }catch (e){
      console.log(e);
    }
  }

  async function sendCodeAgain(event){
    event.preventDefault();
    resetErrors();

    try{
      const response = await sendEmailVerificationCode({email: props.email});
      if(response.success){

      }else{
        handleErrors(response);
      }
    }catch (e){
      console.log(e);
    }


  }

  return (
      <>
        <section className="mx-auto">
          <div className="text-gray-600 font-bold text-lg my-3">Введіть код для підтвердження email</div>
          <form onSubmit={submitHandler} className='rounded-lg p-6 shadow-md  bg-yellow-100'>

            <Input label="Код" field='code'
                   inputType='text'
                   validationMessage={validationMessage}
                   reference={codeInputRef}
                   styleClass='yellow-200-autofill'/>

            {errorMessage && <div className="my-2 text-sm text-red-500">{errorMessage}</div>}

            <div className='flex-center'>
              {isLoading && <Loading styleClass="mr-4"/>}
              <button disabled={isLoading} onClick={sendCodeAgain} type="text"
                      className="text-sm mr-2 text-gray-600 hover:text-gray-800">
                Відправити код ще раз
              </button>
              <button disabled={isLoading} className='submit-btn'>Підтвердити</button>
            </div>
            <div className='flex-center mt-4'>
              {props.backStep &&
                  <button disabled={isLoading} onClick={props.backStep} type="text"
                          className="text-sm mr-2 text-gray-600 hover:text-gray-800">
                    Повернутись до реєстрації
                  </button>}
            </div>
          </form>
        </section>
      </>
  );
}

export default VerifyEmailForm;
