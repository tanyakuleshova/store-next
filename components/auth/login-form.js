import {useRef, useContext} from 'react';
import {AuthContext} from "@/context/auth-context";
import {useAuthApi} from "@/hooks/api-hook";
import Router from "next/router";
import Input from "@/components/elements/input";
import {useHandleResponse} from "@/hooks/handle-response-hook";
import Loading from "@/components/elements/loading";

function LoginForm(props) {
  const auth = useContext(AuthContext);
  const { login, isLoading, error } = useAuthApi();
  const { validationMessage, errorMessage, handleErrors, resetErrors } = useHandleResponse(error);

  const emailInputRef = useRef();
  const passwordInputRef = useRef();

  async function loginUser(email, password){
    try{
      const response = await login({email, password, tmp_id: auth.tmp_id});
      if(response.success){
        auth.login(response.user, response.token);
        Router.push('/')
      }else{
        handleErrors(response);
      }
    }catch (e){
      console.log(e);
    }
  }

  async function submitHandler(event){
    event.preventDefault();
    resetErrors();
    const email = emailInputRef.current.value;
    const password = passwordInputRef.current.value;
    await loginUser(email, password);
  }

  return (
      <>
        <section className="mx-auto">
          <div className="text-gray-600 font-bold text-lg my-3">Вхід</div>
          <form onSubmit={submitHandler} className='rounded-lg p-6 shadow-md bg-yellow-200 form-w'>

            <Input label="Email" field='email'
                   inputType='email'
                   validationMessage={validationMessage}
                   reference={emailInputRef}
                   styleClass='yellow-200-autofill'
                   wrapperStyle='mb-5'
            />

            <Input label="Пароль" field='password'
                   inputType='password'
                   validationMessage={validationMessage}
                   reference={passwordInputRef}
                   styleClass='yellow-200-autofill'
                   wrapperStyle='mb-3'
            />

            <div className="mb-3 text-sm text-red-500" style={{minHeight: '20px'}}>
              {errorMessage && <span>{errorMessage}</span>}
            </div>

            <div className='flex-center'>
              {props.forgotPassword &&
                  <button disabled={isLoading} onClick={props.onclickForgotPassword} type="text"
                    className="text-sm mr-2 text-gray-600 hover:text-gray-800">
                    Забули пароль?
                  </button>}
              <button  disabled={isLoading} className='submit-btn'>Увійти</button>
              <div className='w-6 ml-2'>{isLoading && <Loading/>}</div>
            </div>
          </form>
        </section>
      </>
  );
}

export default LoginForm;
