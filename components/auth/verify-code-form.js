import {useRef, useState} from 'react';
import {useAuthApi} from "@/hooks/api-hook";
import Input from "@/components/elements/input";
import {useHandleResponse} from "@/hooks/handle-response-hook";
import Loading from "@/components/elements/loading";

function VerifyCodeForm(props) {

  const { sendResetPswCode, isLoading, error } = useAuthApi();
  const { validationMessage, errorMessage, handleErrors, resetErrors } = useHandleResponse(error);
  const [ errorMsg, setErrorMsg ] = useState(errorMessage);

  const codeInputRef = useRef();

  async function submitHandler(event){
    event.preventDefault();
    resetErrors();
    let code = parseInt(codeInputRef.current.value);
    console.log(code)
    if(code !== '' && code.toString().length === 4 && typeof code === 'number') {
      props.setCode(codeInputRef.current.value);
      props.nextStep();
    } else {
      setErrorMsg('Код має складатися із 4-х цифр')
    }

  }

  async function sendCodeAgain(event){
    event.preventDefault();
    resetErrors();

    try{
      const response = await sendResetPswCode({email: props.email});
      if(response.success){

      }else{
        handleErrors(response);
      }
    }catch (e){
      console.log(e);
    }


  }

  return (
      <>
        <section className="mx-auto">
          <div className="text-gray-600 font-bold text-lg my-3">Введіть код для скидання паролю</div>
          <form onSubmit={submitHandler} className='rounded-lg p-6 shadow-md bg-yellow-200 form-w'>

            <Input label="Код" field='code'
                   inputType='text'
                   validationMessage={validationMessage}
                   reference={codeInputRef}
                   styleClass='yellow-200-autofill'/>

            <div className="mb-3 text-sm text-red-500" style={{minHeight: '20px'}}>
              {errorMsg && <span>{errorMsg}</span>}
            </div>

            <div className='flex-center'>
              <div className='w-6 mr-2'>{isLoading && <Loading/>}</div>

              <button disabled={isLoading} onClick={sendCodeAgain} type="text"
                      className="text-sm mr-2 text-gray-600 hover:text-gray-800">
                Відправити код ще раз
              </button>
              <button disabled={isLoading} className='submit-btn'>Підтвердити</button>
            </div>
            <div className='flex-center mt-4'>
              {props.backStep &&
                  <button disabled={isLoading} onClick={props.backStep} type="text"
                          className="text-sm mr-2 text-gray-600 hover:text-gray-800">
                    Повернутись до входу
                  </button>}
            </div>
          </form>
        </section>
      </>
  );
}

export default VerifyCodeForm;
