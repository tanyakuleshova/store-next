import {useEffect, useState} from "react";
import {v4 as uuid} from "uuid";
import {usePathname} from "next/navigation";

const Filters = (props) => {

    const path = usePathname()
    const [ attrs, setAttrs ] = useState([]);
    const [ filtersData, setFiltersData ] = useState(props.attrFilters);
    const [ filters, setFilters ] = useState([]);
    const [ ajax, setAjax ] = useState(false);

    useEffect(() => {
        parse_filters()
    }, []);

    const isFilterChecked = (atr_id, atr_val) => {
        let isChecked = false;
        if(attrs[atr_id]){
            attrs[atr_id].forEach(function (item){
                if(item === atr_val.toString()){
                    isChecked = true;
                }
            });
        }
        return isChecked;
    }
    const parse_filters = () => {
        let filters = path.split('/filters-')[1];
        if(filters){
            filters = filters.split('?')[0];

            let arr = filters.split('-');

            let new_filters = [];
            let new_attrs = [];


            arr.forEach(function (item){
                let atr = item.split('_');
                new_attrs[atr[0]] = new_attrs[atr[0]] ? new_attrs[atr[0]] : [];
                new_attrs[atr[0]].push(atr[1]);
            });

            new_attrs.forEach((item, key)=> {
                if(item.length){
                    let f = {'atr_id': key, 'value_ids': item };
                    new_filters.push(f);
                }
            });

            setFilters(new_filters);
            setAttrs(new_attrs);
        }
    }
    const choose_filter = (e) => {
        const _this = e.target;

        let atr_id = _this.dataset.atr_id;
        let atr_val = _this.dataset.atr_val;

        if(_this.checked){
            fill_atr_params(atr_id, atr_val);
        }else{
            remove_atr_params(atr_id, atr_val);
        }
    }

    useEffect(() => {
        if(ajax) {
            let url = get_url();
            history.pushState({}, null, url);
            apply_filters();
        }

    }, [filters, ajax])

    function apply_filters()
    {
        // props.setCurrentPage('1');
        fetch(process.env.API_URL + "/category/products", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',

            },
            body: JSON.stringify({'category_id': props.category_id, 'filters': filters, 'ajax': true, 'page': '1'})
        })
            .then((response) => response.json())
            .then((response) => {
                if (response.success) {
                    // setProducts(response.products);
                    // setCountHandler(response.products.total, response.products.per_page);
                    props.applyCallback(response);
                    setFiltersData(response.filters_data)
                }
            })
    }

    const fill_atr_params = (atr_id, atr_val) => {
        let new_attrs = attrs;
        new_attrs[atr_id] = new_attrs[atr_id] ? new_attrs[atr_id] : [];
        new_attrs[atr_id].push(atr_val);

        let new_filters = [];
        new_attrs.forEach((item, key)=> {
            if(item.length){
                let f = {'atr_id': key, 'value_ids': item };
                new_filters.push(f);
            }
        });

        setFilters(new_filters);
        setAttrs(new_attrs);
        setAjax(true);
    }

    function remove_atr_params(atr_id, atr_val){
        let arr = [];
        let new_attrs = attrs;
        console.log(attrs);
        attrs[atr_id].forEach(function (item){
            if(item !== atr_val){
                arr.push(item);
            }
        });

        if(arr.length){
            new_attrs[atr_id] = arr;
        }else{
            delete new_attrs[atr_id];
        }

        let new_filters = [];
        new_attrs.forEach((item, key)=>{
            if(item.length){
                let f = {'atr_id': key, 'value_ids': item };
                new_filters.push(f);
            }
        });

        setAttrs(new_attrs);
        setFilters(new_filters);
        setAjax(true);
    }

    const get_url = () => {
        let str = '';
        let url = '';

        if(filters){
            filters.forEach((item)=>{
                let atr = item.atr_id;
                item.value_ids.forEach((item)=>{
                    str += '-' + atr +'_'+item;
                });
            });

            // url = "/categories" + "/" + id + "/" + name + '/1/filters' +str;
            url = props.url + '/1/filters' + str;
            if(!filters.length){
                // url = "/categories" + "/" + id + "/" + name + '/' + currentPage;
                url = props.url + "/" + props.currentPage.toString();
                props.setFiltersQueryParam(null);
            }else{
                props.setFiltersQueryParam('filters' + str);
            }
        }

        return url;
    }

    return (<>
        {filtersData && Object.keys(filtersData).map((key) => {
            const filter = filtersData[key];
            return (
                <div key={uuid()}>
                    <h1>{filter.key}</h1>
                    <div className="my-4" style={{maxHeight: "250px", overflowY: "auto"}}>
                        {filter.values && Object.keys(filter.values).map((k) => {
                            const value = filter.values[k];
                            return (
                                <div key={uuid()} className="text-md">
                                    <input onChange={(event)=> choose_filter(event)}
                                           data-atr_id={filter['atr_id']}
                                           data-atr_val={k}
                                           className="filter-checkbox"
                                           type="checkbox"
                                           checked={isFilterChecked(filter['atr_id'], k)}
                                    />
                                    <span className="ml-2 text-md">{value['name']} ({value['doc_count'] ?? ''})</span>
                                </div>)
                        })}
                    </div>
                </div>
            );
        })}
    </>);
}
export default Filters;
