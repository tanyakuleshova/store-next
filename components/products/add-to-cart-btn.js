import {useContext, useRef, useState} from "react";
import {useCartApi} from "@/hooks/api-hook";
import {AuthContext} from "@/context/auth-context";
import Image from "next/image";
import {useAddToCart} from "@/hooks/add-to-cart-hook";

const AddToCartBtn = (props) => {

  const [emojiIndex, setEmojiIndex] = useState(null);
  const emojiRef = useRef();
  const emojiCount = 3;
  const { addToCart } = useAddToCart();
  const showEmoji = () => {
    let i = Math.floor(Math.random() * 4);
    setEmojiIndex(i);
    emojiRef.current.style.opacity = 1;
    emojiRef.current.style.top = "-55px"
    setTimeout(()=> {
      emojiRef.current.style.opacity = 0;
      emojiRef.current.style.top = "-20px"
      setEmojiIndex(null);
    },500);
  }
  const imageSources = [];

  for (let i = 0; i <= emojiCount; i++) {
    imageSources.push(`/images/smiles/${i}.svg`);
  }

  return (
      <>
        <div className="relative">
          <button disabled={emojiIndex !== null} onClick={()=> { addToCart(props.id, props.qty); showEmoji()}}
                  className="submit-btn shadow-md" type="text">{props.name}</button>
          <div ref={emojiRef} className="absolute w-6 h-6 right-16" style={{ transition: "top 1s", top: "-20px" }}>
            {imageSources.map((src, index) => (
                <Image
                    key={index} src={src} alt={`Smile ${index}`} width={20} height={20}
                    className={`${emojiIndex === index ? 'opacity-1' : 'opacity-0'} absolute`}
                />
            ))}
          </div>
        </div>
      </>
  )
}

export default AddToCartBtn;
