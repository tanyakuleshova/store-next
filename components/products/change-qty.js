import { useState } from "react";
import {useAddToCart} from "@/hooks/add-to-cart-hook";

const ChangeQty = (props) => {
  const [local_qty, local_setQty] = useState(props.qty);
  let qty = props.setQty ? props.qty : local_qty;
  let setQty = props.setQty ? props.setQty : local_setQty;
  const { addToCart, changeCartItemQty } = useAddToCart();
  const changeQty = async (sign) => {
    if(sign === "-"){
      if(qty > 1){
        setQty(qty - 1);
      }
    }
    if(sign === "+"){
      if(qty < 20){
        setQty(qty + 1);
      }
    }
    if (props.product_id){
      const res = await addToCart(props.product_id, 1, sign);
      if(res.success){
        props.callback(res);
      }
    }
  }

  const changeManually = async (value) => {
    let newVal = value;
    if (newVal < 1 || newVal > 1000 || !newVal) {
      newVal = 1;
    }

    if(props.item_id){
      const res = await changeCartItemQty(props.item_id, newVal);
      console.log(res)
      if (res.success){
        props.callback(res);
      }
    }else{
      setQty(newVal);
    }

  }

  const setCartQty = (value) => {
    setQty(value);
  }

  return (
      <>
        <div className={`flex-center font-bold ${props.styleClass}`}>
          <button onClick={()=> changeQty("-")}
                  className={`submit-btn rounded-full shadow-md ${props.btnClass}`} type="text">-</button>
          <input
              type="text"
              onBlur={(e) => changeManually(e.target.value)}
              onChange={(e)=> {setCartQty(e.target.value)}}
              className="classic-input  w-12 mx-2 text-center border-b-2 border-transparent focus:border-yellow-600 outline-none"
              value={qty}
          />
          <button onClick={()=> changeQty("+")}
                  className={`submit-btn rounded-full shadow-md ${props.btnClass}`} type="text">+</button>
        </div>
      </>
  )
}

export default ChangeQty;
