import Image from "next/image";
import AddToCartBtn from "@/components/products/add-to-cart-btn";
function ProductCard(props) {

    return <>
        <div itemProp="itemListElement" itemScope itemType="https://schema.org/Product" className="w-1/4 p-4">
            <div className={`${props.bg ?? 'bg-white'} rounded-md`}>
                <a href={`/product/${props.data?.id}/${props.data?.lang?.slug}`} className="w-full">
                    <Image itemProp="image" height={200} width={250}
                           className="mx-auto" src={ props.data?.s3path || props.url}
                           alt={props.data?.lang?.name ?? 'test'}
                       style={{maxHeight: 200, maxWidth: 250}}  />
                    <div className="py-2 text-center font-bold text-gray-700">
                        <span itemProp="name">{props.data?.lang?.name}</span>
                    </div>
                </a>
                <div className="">
                    <div className="flex items-center justify-between mt-8">
                        <div itemProp="offers" itemScope itemType="https://schema.org/Offer" className="flex-center">
                            <div className="text-lg font-bold">
                                <span itemProp="price" content={props.data?.price}>
                                    {props.data?.price}
                                </span>
                            </div>
                            <div className="ml-2 text-sm">
                                <span itemProp="priceCurrency" content="UAH">грн</span>
                            </div>
                            <link itemProp="availability" href="https://schema.org/InStock" content="In stock"/>
                        </div>
                        <AddToCartBtn id={props.data?.id} qty="1" name="У кошик"/>
                    </div>
                </div>
            </div>
        </div>
    </>;
}

export default ProductCard;
