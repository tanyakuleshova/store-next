import {useRef, useState} from "react";
import {v4 as uuid} from "uuid";

const Carousel = (props) => {
    const [ currentUrl, setCurrentUrl ] = useState(props.mainUrl);
    const [ currentIndex, setCurrentIndex ] = useState(0);

    const imageRef = useRef();
    const setNextImage = (sign) => {

        if(sign === "+"){
            if(currentIndex < props.images.length){
                setCurrentIndex(currentIndex + 1);
                setCurrentUrl(props.images[currentIndex + 1])
            }else if(currentIndex >= props.images.length){
                setCurrentIndex(0);
                setCurrentUrl(props.images[0])
            }
        }
        if(sign === "-"){
            if(currentIndex > 0 && currentIndex <= props.images.length){
                setCurrentIndex(currentIndex - 1);
                setCurrentUrl(props.images[currentIndex - 1])
            }else if(currentIndex === 0){
                setCurrentIndex(props.images.length-1);
                setCurrentUrl(props.images[props.images.length-1])
            }
        }
    }
    return (
        <>
            <div className="flex-center">
                {props.images.length > 1 && <div className="">
                    <button onClick={()=>setNextImage("-")} type="text"
                            className=" bg-yellow-400 rounded-2xl rounded-full shadow-lg p-2">
                        <img src="/images/icons/arrow-b-left.svg" alt="arrow-left" style={{width: '10px', height: '10px'}}/>
                    </button>
                </div>}
                <div ref={imageRef} style={{height: '550px', width: '550px', transition: 'opacity 0.2s'}}>
                    <img
                        itemProp="image"
                        className="p-4 " src={currentUrl}
                        alt={props.alt}
                        style={{maxHeight: '100%', maxWidth: '100%'}}
                    />
                </div>
                {props.images.length > 1 && <div className="relative">
                    <button onClick={()=>setNextImage("+")} type="text" className="bg-yellow-400 rounded-2xl rounded-full shadow-md p-2">
                        <img src="/images/icons/arrow-b-right.svg" alt="arrow-right" style={{width: '10px', height: '10px'}}/>
                    </button>
                </div>}
            </div>
            <div className="flex-start flex-wrap p-4">
                {props.images && props.images.length > 1 && props.images.map(item => (
                    <div key={uuid()} className=" p-2" style={{width: "100px", height: "100px"}}>
                        <img
                            onClick={()=>setCurrentUrl(item)}
                            itemProp="image"
                            className="cursor-pointer" src={ item }
                            alt={props.alt}
                            style = {{ height: '100%', width: '100px'}}
                        />
                    </div>
                ))}
            </div>
        </>
    )
}

export default Carousel;
