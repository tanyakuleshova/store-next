import {useRef, useState} from "react";

const Description = (props) => {

    const descriptionRef  = useRef(null);
    const [ showDescription, setShowDescription ] = useState(props.defaultMode);
    const toggleShowDescription = () => {
        descriptionRef.current.style.height = showDescription ? '5rem' : '100%'
        setShowDescription(prev => !prev);
    }
    return (
        <>
            <div
                style={{ height: props.defaultMode ? '100%' : '5rem' }}
                ref={descriptionRef}
                className={` overflow-hidden ${props.styleClass}`}
                dangerouslySetInnerHTML={{ __html:props.description }}
            >
            </div>

            <button
                onClick={toggleShowDescription}
                type="text"
                className="submit-btn mt-6 text-xs">
                {showDescription ? 'Приховати' : 'Детальніше'}
            </button>
        </>
    )
}

export default Description;
