import Link from "next/link";
import Image from "next/image";
function BlogCard(props) {
    return <>
        <div className="w-1/4 p-4">
            <div className="bg-white rounded-md  w-full shadow-md" style={{"height": "450px"}}>
                <div className="w-full rounded-t-md" style={{
                    backgroundImage: `url(${props.url})`,
                    width: '100%',
                    height: '180px',
                    backgroundSize: 'cover',
                    backgroundRepeat: 'no-repeat'
                }}>
                </div>
                <div className="p-4">
                    <div className="text-sm text-gray-500 mb-2">Author Name</div>
                    <div className="font-bold text-gray-500">
                        Lorem Ipsum
                    </div>
                    <div className="w-full border-b my-4"></div>
                    <div className="text-gray-500 text-sm">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                        et dolore magna aliqua.
                    </div>
                    <div className="flex items-center justify-between mt-8">
                        <button type="text" className="bg-blue-400 rounded-2xl text-sm px-6 py-2 text-white font-bold">Read</button>
                    </div>
                </div>
            </div>
        </div>
    </>;
}

export default BlogCard;
