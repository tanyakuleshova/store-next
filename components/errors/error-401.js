import Link from "next/link";

function Error401() {
    return <>
        <div className='text-center text-4xl my-4 text-gray-600'>401</div>
        <div className='text-center text-4xl my-4 text-gray-600'>Unauthorized</div>
        <Link href='/login' className='flex-center text-2xl my-4 text-yellow-500'>Login</Link>
    </>;
}

export default Error401;
