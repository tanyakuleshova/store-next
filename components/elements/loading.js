import Link from "next/link";
import {v4 as uuid} from "uuid";

function Loading(props) {
    return <div className={`w-6 h-6 ${props.styleClass}`}>
        <img className={`h-6 w-6`} src="/images/loading/rolling-yellow.svg" alt=""/>
    </div>;
}

export default Loading;
