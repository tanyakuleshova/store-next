import { v4 as uuid } from 'uuid';
const Pagination = (props) => {

   const pageCount = props.pageCount;
   const currentPage = props.currentPage;
   const setCurrentPage = props.setCurrentPage;
   const maxPaginationButtons = 3;
   // let url = props?.url?.replace(/\/[^/]+$/, '');
   let url = props?.url;

   if(pageCount == 1){
       return "";
   }
    const prevPage = () => {
        if(currentPage > 1 ){
            return currentPage-1;
        }
        return null;
    }
    const nextPage = () => {
        if(currentPage < pageCount){
            return parseInt(currentPage)+1
        }
        return null;
    }
    const pageHandler = (page) => {
        setCurrentPage(page);
    }

    return (
        <div className="flex items-center text-sm">
            {pageCount > 1 && prevPage() ? <a href={props.filters ? `${url}/${prevPage()}/${props.filters}` : `${url}/${prevPage()}` }>
                <img src="/images/icons/arrow-t-left.svg" className="mr-2 cursor-pointer" alt=""/>
            </a>: "" }
            {[...Array(pageCount)].map((x, i) =>
                (i === maxPaginationButtons && pageCount > 6) ? (currentPage === 3 || currentPage === 5 || currentPage === 4 ?
                        <div key={uuid()}>
                            <a  href={props.filters ? `${url}/${i+1}/${props.filters}` : `${url}/${i+1}`} className={`${currentPage == 4 ? 'bg-blue-400' : 'bg-gray-300'} 
                            px-2 py-1 cursor-pointer rounded-md mr-2 text-black`}>
                                4
                            </a>
                            <div className="px-1 text-gray-500 mr-2">...</div>
                        </div> : <div key={uuid()} className="px-1 text-gray-500 mr-2">...</div>) :
                        ((i+1) <= maxPaginationButtons || (i+1) > pageCount - maxPaginationButtons ||
                        (currentPage > maxPaginationButtons && pageCount > maxPaginationButtons * 2
                            && (((i+1) >= currentPage && (i+1) <= currentPage + maxPaginationButtons))
                            || (i<=currentPage && i>= currentPage-maxPaginationButtons))) ?

                            <div key={uuid()}>{(i+1) != currentPage ?
                                <a  href={props.filters ? `${url}/${i+1}/${props.filters}` : `${url}/${i+1}`}
                                    className='bg-yellow-300 pagination-button cursor-pointer' key={uuid()}>{i+1}</a> :
                                <div className="bg-yellow-500 pagination-button" key={uuid()}>{i+1}</div>}</div>
                        : ""
            )}
            {pageCount > 1 && nextPage() ? <a href={props.filters ? `${url}/${nextPage()}/${props.filters}` : `${url}/${nextPage()}`}>
                <img src="/images/icons/arrow-t-right.svg" className="cursor-pointer" alt=""/>
            </a> : "" }
        </div>
    )
}

export default Pagination;
