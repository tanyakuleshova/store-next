import { createPortal } from 'react-dom'
import Backdrop from './backdrop';

const ModalOverlay = props => {
    const content = (
        <div className={`modal ${props.className}`}>
            <header className={`${props.headerClass}`}>
                <h2>{props.header}</h2>
            </header>
            <div>
                <div className={`${props.contentClass}`}>
                    {props.children}
                </div>
                <footer className={`${props.footerClass}`}>
                    {props.footer}
                </footer>
            </div>
        </div>
    );
    return createPortal(content, document.getElementById('modal-hook'));
};

const Modal = props => {
    return (
        <>
            {props.show && <Backdrop onClick={props.onCancel} />}
            {props.show && <div>
                <ModalOverlay {...props} />
            </div>}
        </>
    );
};

export default Modal;
