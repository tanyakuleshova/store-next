import Link from "next/link";
import {v4 as uuid} from "uuid";
import {useEffect, useState} from "react";

function Input(props) {
    const [validationMsg, setValidationMsg] = useState([]);

    useEffect(() => {
        setValidationMsg(props.validationMessage);
    }, [props.validationMessage]);
    return <>
        <div className={`text-gray-600 ${props.wrapperStyle}`}>
            <div className='text-sm font-bold mb-1'>{props.label}</div>
            <input className={` ${validationMsg && validationMsg[props.field] ? 'border-red-500': ''} classic-input w-full ${props.styleClass}`}
                   type={`${props.inputType ?? 'text'}`}
                   name={`${props.field}`}
                   onChange={() => {setValidationMsg([])}}
                   ref={props.reference} />
            {validationMsg && <div className="my-2 text-sm text-red-500 w-full">
                {validationMsg[props.field] && validationMsg[props.field].map(error => <div key={uuid()}>{error}</div>)}
            </div>}
        </div>
    </>;
}

export default Input;
