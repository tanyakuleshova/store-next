import {v4 as uuid} from "uuid";

function Bread(props) {

    return <>
        {props.breadCrumbs && <ol itemScope itemType="https://schema.org/BreadcrumbList"
                                  className={`flex-start text-md ${props.styleClass}`}>
            {props.breadCrumbs.map((item, k) => (
              <li itemProp="itemListElement" itemScope itemType="https://schema.org/ListItem" key={uuid()}>
                  {(k !== props.breadCrumbs.length - 1 || props.product)  ?
                      <>
                          <a itemProp="item" href={`/categories/${item.id}/${item.lang?.slug}/1`}>
                              <span itemProp="name">{item.lang?.name}</span>
                              <span className="mx-2">/</span>
                          </a>
                          <meta itemProp="position" content={k+1}/>
                      </> : <>
                          <div itemProp="item">
                              <span itemProp="name">{item.lang?.name}</span>
                          </div>
                          <meta itemProp="position" content={k+1}/>
                      </>
                  }
              </li>
            ))}
            {props.product ? <li itemProp="itemListElement" itemScope itemType="https://schema.org/ListItem" key={uuid()}>
                <div itemProp="item">
                    <span itemProp="name">{props.product.lang?.name}</span>
                </div>
                <meta itemProp="position" content={props.breadCrumbs.length}/>
            </li> : ""}
        </ol>}
    </>;
}

export default Bread;
