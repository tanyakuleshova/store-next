import Link from "next/link";
import Image from "next/image";
import {AuthContext} from "@/context/auth-context";
import {useContext, useEffect, useState} from "react";
import {useAuthApi, useCartApi} from "@/hooks/api-hook";
import {useRouter} from "next/router";
function Header(props) {
    const auth = useContext(AuthContext);
    const [showMenu, setShowMenu] = useState(false);

    const icon = auth.userData && auth.userData.icon_url ? auth.userData.icon_url : "/images/default_user.jpg";
    const router = useRouter();

    const { logout, isLoading, error } = useAuthApi();
    const { cartQty } = useCartApi();
    async function logoutUser(event) {
        event.preventDefault();
        try{
            await logout({tmp_id: auth.tmp_id});
            auth.logout();
        }catch (e){
            console.log(e);
        }
    }

    useEffect( () => {
        if(auth.tmp_id){
           cartQty().then(r => {
               if(r.success){
                   auth.setCartCount(r.qty);
               }
           })
        }
    }, [auth.tmp_id]);

    return <>
        <div className="p-4 mx-auto" style={{"maxWidth": "1400px"}}>
            <div className="text-black flex items-center justify-between">
                <div className="font-bold text-2xl flex items-center justify-start">
                    <Image height={20} width={20} src="/images/icons/star-black.svg" alt=""/>
                    <div className="ml-2">CATO PLATO</div>
                </div>
                <div className="flex items-center justify-between text-lg font-bold">
                    <div className="ml-4 h-12"></div>
                    {router.pathname !== "/" && <Link className="ml-4" href='/'>Головна</Link>}
                    <div className="ml-4">Меню</div>
                    {!auth.isLoggedIn && router.pathname !== "/login" ? <Link className="ml-4" href='/login'>Вхід</Link> : ""}
                    {auth.isLoggedIn && <div className="relative">
                        <div onClick={()=> setShowMenu(true)} className="w-12 h-12 rounded-full ml-4 text-md" style={{
                            backgroundImage: `url(${icon})`,
                            backgroundSize: 'cover',
                            backgroundRepeat: 'no-repeat'
                        }}></div>
                        {showMenu && <div className="absolute top-12 right-0 mt-2 bg-white text-black p-4 rounded-md">
                            <div className="pointer" onClick={(e)=>logoutUser(e)}>Вийти</div>
                            <div>Профіль</div>
                        </div>}
                    </div>}
                    <a href={router.pathname !== "/cart" ? "/cart" : "#"} className="relative ml-4">
                        <Image height={50} width={50} src='/images/icons/cart.svg' alt='cart'/>
                        {auth.cartCount && auth.cartCount > 0 ? <div className='absolute right-0 top-0 text-xs p-1 bg-gray-600 text-white rounded-full'>
                            {auth.cartCount}
                        </div> : ''}
                    </a>
                </div>
            </div>
        </div>
    </>;
}

export default Header;
