import { Fragment } from 'react';
import Header from "@/components/layout/header";
import Footer from "@/components/layout/footer";
import {useRouter} from "next/router";

function Layout(props) {
    const router = useRouter();
  return (
    <Fragment>
        <div id="backdrop-hook"></div>
        <div id="modal-hook"></div>
        <div className="bg-yellow-100">

        {/*<div className="bg-gradient-to-b from-yellow-100 from-40% to-red-400 to-90%">*/}
        {/*<div className="bg-gradient-to-b from-yellow-200 from-40% to-yellow-100 to-90%">*/}
            {/*<div className="bg-gradient-to-b from-yellow-400 absolute top-0 left-0 right-0" style={{"height": "1000px"}}></div>*/}
            {/*<div className="bg-yellow-400 absolute top-0 left-0 right-0" style={{"height": "80px"}}></div>*/}
            {router.pathname !== "/login" && <div className="bg-gradient-to-b from-yellow-400 absolute top-0 left-0 right-0" style={{"height": "800px"}}></div>}
            <div className="relative z-50">
                {router.pathname !== "/login" && <Header/>}
                {/*<div className='border-b-2 border-b-white'></div>*/}
                <main className="main-bloc">{props.children}</main>
                {/*<div className='border-b-2 border-b-white'></div>*/}
                {/*<Footer/>*/}
            </div>
        </div>
    </Fragment>
  );
}

export default Layout;
