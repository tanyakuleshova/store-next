import Link from "next/link";
import Image from "next/image";
import {AuthContext} from "@/context/auth-context";
import {useContext, useState} from "react";
import {useAuthApi} from "@/hooks/api-hook";
import {useRouter} from "next/router";
function SmallHeader(props) {

    const router = useRouter();

    return <>
        <div className="p-4 mx-auto">
            <div className="text-black flex items-center justify-between">
                <div className="font-bold text-2xl flex items-center justify-start">
                    {/*<Image height={20} width={20} src="/images/icons/star-black.svg" alt=""/>*/}
                    <div className="ml-2 text-3xl" style={{textShadow: "-5px 3px 2px #fea600"}}>SURPRISE</div>
                </div>
                <div className="flex items-center justify-between text-lg font-bold">
                    <div className="ml-4 h-12"></div>
                    {router.pathname !== "/" && <Link className="ml-4" href='/'>Main</Link>}
                    <div className="ml-4">Menu</div>
                    <div className="ml-4">Sales</div>
                </div>
            </div>
        </div>
    </>;
}

export default SmallHeader;
