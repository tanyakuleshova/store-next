/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env: {
    API_URL: process.env.API_URL,
    API_URI: process.env.API_URI,
    S3_URL: process.env.S3_URL,
  },
  images: {
    domains: [
      // process.env.S3_URL,
      // process.env.API_URI
      'whalebase-store.s3.eu-central-1.amazonaws.com', 'whalebase_api.local'
    ],
  },
}

module.exports = nextConfig
